const express = require('express');
const app = express();
const port = process.env.PORT || 3000;


const userController = require('./controllers/userController.js');
const authController = require('./controllers/authController.js');



app.listen(port);
const bodyParser = require('body-parser');
app.use(bodyParser.json());
console.log("Api escuchando en el BEEEEEP "+port);



app.get('/apitechu/v1/hello',

  function(req,res){
    //req request res response
    console.log("GET /apitechu/v1/hello'");
    res.send({"msg" : "Hola desde apitechu AUTOMATICO OU YEAH pete can pete jal pete gromenawer"});
  }
);
app.post('/apitechu/v1/monstruo/:p1/:p2',

  function(req,res){
    //req request res response
    console.log("POST /apitechu/v1/monstruo/:p1/p2");
    //res.sendFile('usuarios.json',{root:__dirname});
    console.log("Parametros");
    console.log(req.params);

    console.log("QueryString");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);

app.get("/apitechu/v1/users",userController.getUsersV1);

app.post('/apitechu/v1/users',userController.createUserV1);

app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);

app.post('/apitechu/v1/login',authController.userLoginV1);

app.post('/apitechu/v1/logout',authController.userLogoutV1);

/*  app.get('/apitechu/v1/users/?top&&?count',


  );*/
app.post('/apitechu/v2/login',authController.userLoginV2);
app.post('/apitechu/v2/logout/:id',authController.userLogoutV2);
app.get("/apitechu/v2/users/",userController.getUsersV2);
app.get("/apitechu/v2/users/:id",userController.getUsersByIDV2);
app.post('/apitechu/v2/users',userController.createUserV2);
