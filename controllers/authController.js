const io = require('../io');
const requestJson = require('request-json');
const crypt = require('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmt/collections/";
const mLabAPIKey = "apiKey=wkJJQMZp7_MzjG9pzqflNK9rTcZ5yhlk";

function userLoginV1(req,res){
  console.log("POST /apitechu/v1/login'");
  //res.sendFile('usuarios.json',{root:__dirname});
  //console.log(req.headers);
  console.log("email es " + req.body.email);
  var users = require('../usuarios.json');

  for (user of users){
    //var usuario = users.valueOf(i);
    if (user!=null && user.email == req.body.email){
      //borro
      console.log("el usuario "+ user.first_name +" "+user.last_name + " se ha logado");
      user.logged = true;//esto no vale porque es un poco ñapa suponiendo que viene siempre con el mismo orden?
      break;
    }
  }
  if(user.logged !=null){
    //

    if (user.password == req.body.password){
      io.writeUserDataToFile(users);
      console.log("El usuario esta logado");
      var respuesta = {
        "msg" : "Login Correcto",
        "id"  : user.id
      };
      res.send(respuesta);
    }else{
      console.log("El usuario no esta logado");
      res.send("msg : login incorrecto");
    }
  }else{
    //el usuario no esta logado
    console.log("El usuario no existe");
    res.send("msg : login incorrecto");
  }
}

function userLogoutV1(req,res){
  console.log("POST /apitechu/v1/logout'");
  //res.sendFile('usuarios.json',{root:__dirname});
  //console.log(req.headers);
  console.log("id para deslogar es " + req.body.id);
  var users = require('../usuarios.json');

  for (user of users){
    //var usuario = users.valueOf(i);
    if (user!=null && user.id == req.body.id){
      //borro
      console.log("el usuario "+ user.first_name +" "+user.last_name + " se ha deslogado");
      delete user.logged;//esto no vale porque es un poco ñapa suponiendo que viene siempre con el mismo orden?
      break;
    }
  }
  if(user.id == req.body.id){
    //

      io.writeUserDataToFile(users);
      console.log("El usuario esta deslogado");
      var respuesta = {
        "msg" : "Logout Correcto",
        "id"  : user.id
      };
      res.send(respuesta);
  }else{
      console.log("El usuario no esta logado");
      res.send("msg : logout incorrecto");
  }

}

function userLoginV2(req,res){
  console.log("POST /apitechu/v2/login'");
  //res.sendFile('usuarios.json',{root:__dirname});
  //console.log(req.headers);
  console.log("email es " + req.body.email);

  var query = 'q={"email": "' + req.body.email + '"}';

  console.log(query);

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?"+ query + "&" + mLabAPIKey,
     function(err,resMLab,bodyGet){
       console.log("body es " + bodyGet);
       if(err) {
          var response = {
           "msg" : "Error obteniendo usuario"
          }
         res.status(500);
       } else {
         if (bodyGet.length > 0) {
           var response = bodyGet[0];
           //
            // aqui haría el put para logar.
            console.log("Entro para actualizar el usuario " + response);
            if(crypt.checkPassword(req.body.password,response.password)){
              console.log("PASSWORD CORRECTA");
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("user?"+ query + "&" + mLabAPIKey,JSON.parse(putBody),
                 function(err,resMLab2,bodyPUT){
                    console.log("hago put");
                    if(err){
                      console.log("error al actualizar");
                    }else{
                      var response = {
                        "msg" : "Usuario con id "+ bodyGet[0].id +" logado"
                      };
                      res.send(response);
                    }
                 }
              );
            } else{
                console.log("PASSWORD INCORRECTA");
                var response = {
                  "msg" : "PASSWORD INCORRECTA"
                };
                res.send(response);

            }

         } else {
           var response = {
             "msg" : "Usuario no encontrado"
           }
           res.status(404);
           res.send(response);
         }
       }

     }
   );
   //una vez que obtengo el usuario tengo que hacer el post de login


}
function userLogoutV2(req,res){
  console.log("POST /apitechu/v2/logout/:id'");
  //res.sendFile('usuarios.json',{root:__dirname});
  //console.log(req.headers);
  console.log("id para deslogar es " + req.params.id);
  var query = 'q={"id": ' + req.params.id + '}';// al id no se le ponen comillas

  console.log(query);

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?"+ query + "&" + mLabAPIKey,
     function(errGET,resMLabGET,bodyGET){
       console.log("la peticion es" +resMLab);
       if(errGET) {
          var response = {
           "msg" : "Error obteniendo usuario"
          }
         res.status(500);
       } else {
         if (bodyGET.length > 0) {
           var response = bodyGET[0];
           //
            // aqui haría el put para logar.
            console.log("Entro para actualizar el usuario " + response);
              if(bodyGET[0].logged){

                var putBody = '{"$unset":{"logged":""}}';//esto es un string y hay que convertirlo a jason
                httpClient.put("user?"+ query + "&" + mLabAPIKey,JSON.parse(putBody),
                   function(errPUT,resMLabPUT,bodyPUT){
                      console.log("hago put");
                      if(errPUT){
                        console.log("error al actualizar");
                      }else{
                        var response = {
                          "msg" : "Usuario con id "+ bodyGET[0].id +" DESlogado"
                        };
                        res.send(response);
                      }
                   }
                );
              }else{
                var response = {
                  "msg" : "Usuario con id no esta logado"
                };
                res.send(response);
              }

         } else {
           var response = {
             "msg" : "Usuario no encontrado"
           }
           res.status(404);
           res.send(response);
         }
       }

     }
   );


}

module.exports.userLoginV1 = userLoginV1;
module.exports.userLoginV2 = userLoginV2;
module.exports.userLogoutV1 = userLogoutV1;
module.exports.userLogoutV2 = userLogoutV2;
