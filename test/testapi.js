const mocha = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

var should = chai.should();

var server = require('../server');

describe('First unit test',
  function() {
    it('Test that Duckduckgo works',
      function(done){
        chai.request('http://www.duckduckgo.com')
          .get('/')
          .end(
            function(err,res) {
              console.log("Request has finished");
              //console.log(err);
              //console.log(res);
              res.should.have.status(200);//asercion
              done();
            }
          )
      }

    )

  }
);
describe('First unit test',
  function() {
    it('Test that Apitechu works',
      function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello')
          .end(
            function(err,res) {
              console.log("Request has finished");
              //console.log(err);
              //console.log(res);
              res.should.have.status(200);//asercion
              done();
            }
          )
      }

    ),
    it('Test that Apitechu devuelve una lista de usuarios correcta',
      function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
            function(err,res) {
              console.log("Request has finished");
              //console.log(err);
              //console.log(res);
              res.should.have.status(200);//asercion
              res.body.users.should.be.a("array");
              for (user of res.body.users){
                user.should.have.property('email');
                user.should.have.property('password');
              }
              done();
            }
          )
      }

    )
  }
)
