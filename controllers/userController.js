const io = require('../io');
const requestJson = require('request-json');
const crypt = require('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmt/collections/";
const mLabAPIKey = "apiKey=wkJJQMZp7_MzjG9pzqflNK9rTcZ5yhlk";


function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");
  console.log(req.query);

  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
     users.slice(0, req.query.$top) : users;

  res.send(result);
}
function createUserV1(req,res){
  //req request res response
  console.log("POST /apitechu/v1/users'");
  //res.sendFile('usuarios.json',{root:__dirname});
  //console.log(req.headers);
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  }
  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDataToFile(users);
  console.log("Usuario añadido con éxito");
  res.send({"msg":"Usuario Añadido Con éxito"});
}
function createUserV2(req,res){
  //req request res response
  console.log("POST /apitechu/v2/users'");
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  }
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");
  httpClient.post("user?"+ mLabAPIKey,newUser,
     function(err,resMLab,body){
       console.log("usuario creado con exito");
       res.send({"msg":"Usuario creado con éxito"});
     }
   );

}

function deleteUserV1(req,res){
  //req request res response
  console.log("DELETE /apitechu/v1/users:id'");
  console.log("id es para borrar:" + req.params.id);
  var users = require('../usuarios.json');
  //var users = usersJson.Parse();
  //users.splice(req.params.id -1,1);
  var idBorrar = req.params.id;
  //var i=0;
  for (user of users){
    //var usuario = users.valueOf(i);
    if (user!=null && user.id == idBorrar){
      //borro
      console.log("borro el id "+ user.id + "el valor de la variable es " +idBorrar );
      delete users[user.id-1];//esto no vale porque es un poco ñapa suponiendo que viene siempre con el mismo orden?
      break;
    }

  }
  console.log("Usuario borrado");
  io.writeUserDataToFile(users);
  res.send({"msg":"Usuario borrado Con éxito"});
}

function getUsersByIDV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");
  httpClient.get("user?"+ query + "&" + mLabAPIKey,
     function(err,resMLab,body){
       console.log("body es " + body);
       if(err) {
          var response = {
           "msg" : "Error obteniendo usuario"
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           var response = body[0];
         } else {
           var response = {
             "msg" : "Usuario no encontrado"
           }
           res.status(404);
         }
       }
       res.send(response);
     }
   );
}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users/");

  //var id = req.params.id;
  //var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  //console.log("Cliente creado");
  httpClient.get("user?"+ mLabAPIKey,
     function(err,resMLab,body){
       console.log("body es " + body);
       if(err) {
          var response = {
           "msg" : "Error obteniendo usuario"
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           var response = body;
         } else {
           var response = {
             "msg" : "Usuario no encontrado"
           }
           res.status(404);
         }
       }
       res.send(response);
     }
   );
}



module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersByIDV2 = getUsersByIDV2;
module.exports.getUsersV2 = getUsersV2;
